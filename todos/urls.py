from django.urls import path
from todos.views import todo_list, todo_details, todo_create, todo_update, todo_delete, todo_task, todo_update_task

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_details, name="todo_list_detail"),
    path("create/", todo_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_update, name="todo_list_update"),
    path("<int:id>/delete/", todo_delete, name="todo_list_delete"),
    path("items/create/", todo_task, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_update_task, name="todo_item_update")
]
